flags = {
        "FIN" : 0x01,
        "SYN" : 0x02,
        "RST" : 0x04,
        "PSH" : 0x08,
        "ACK" : 0x10,
        "URG" : 0x20,
        "ECE" : 0x40,
        "CWR" : 0x80,
}


ports = {
    "srv_port" : 55556,
    "cli_port" : 55555,
}

tcp_vals = {
    "ISN" : 1000,
    "TSval" : 12345,
}

sizes = {
    "min_size" : 20,
}