import sys
import socket
import fcntl, struct, os


def get_timestamp_val(options_list):
    for opt,val in options_list:
        if opt == 'Timestamp':
            return val[0]


def get_host_ip(iface):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', "wlan0")
    )[20:24])
