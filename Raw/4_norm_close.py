from scapy.all import IP, TCP, sr1, sniff, send
import sys
import socket
import fcntl, struct, os
import random

sys.path.insert(0,"../Common-Functions")
from defns import *
from common_fns import *
import common_fns


def get_timestamp_val(options_list):
    for opt,val in options_list:
        if opt == 'Timestamp':
            return val[0]


def get_filter(iface):
    host_ip = get_host_ip(iface)
    filter_items = ["tcp and port", str(ports["srv_port"]), "and host", host_ip]
    return " ".join(filter_items)


def main():
    iface = 'wlan0'
    if len(sys.argv) == 2:
        iface = sys.argv[1]

    srv_addr = "10.10.10.4"
    if len(sys.argv) == 3:
        srv_addr = sys.argv[2]

    cli_ip = get_host_ip(iface)
    cli_port = int(50000 + (1000 * random.random()))
    filter = get_filter(iface)

    print "\nSending SYN to Server ..."

    ip_packet = IP()
    ip_packet.src = cli_ip
    ip_packet.dst = srv_addr

    tcp_packet = TCP()
    tcp_packet.dport = ports["srv_port"]
    tcp_packet.sport = cli_port
    tcp_packet.flags = "S"
    tcp_packet.seq = tcp_vals["ISN"]
    tcp_packet.options = [ ('Timestamp', (tcp_vals["TSval"], 0)) ]
    t = ip_packet/tcp_packet
    send(t, verbose=False)

    rep = ""
    while True:
        rep = sniff(iface="wlan0", filter=get_filter(iface), count=1)
        if rep[0][TCP].flags & flags["SYN"] == flags["SYN"]:
            break

    print "Received SYN-ACK ..."
    tcp_packet.flags = "A"
    tcp_packet.ack = rep[0][TCP].seq + 1
    tcp_packet.seq = tcp_vals["ISN"] + 1
    tcp_packet.options = [ ('Timestamp', (tcp_vals["TSval"], get_timestamp_val(rep[0][TCP].options))) ]
    t = ip_packet/tcp_packet
    print "Sending Ack for SYN-ACK ...\n"
    send(t, verbose=False)

    while True:
        rep = sniff(iface="wlan0", filter=get_filter(iface), count=1)
        if rep[0][TCP].flags & flags["FIN"] == flags["FIN"]:
            break
        
    print "Received FIN, sending ACK ...\n"
    tcp_packet.flags = "A"
    tcp_packet.ack = rep[0][TCP].seq + 1
    tcp_packet.seq = tcp_vals["ISN"] + 1
    tcp_packet.options = [ ('Timestamp', (tcp_vals["TSval"], get_timestamp_val(rep[0][TCP].options))) ]
    t = ip_packet/tcp_packet
    send(t, verbose=False)


    tcp_packet.flags = "FA"
    tcp_packet.ack = rep[0][TCP].seq + 1
    tcp_packet.seq = tcp_vals["ISN"] + 1
    tcp_packet.options = [ ('Timestamp', (tcp_vals["TSval"], get_timestamp_val(rep[0][TCP].options))) ]
    t = ip_packet/tcp_packet
    print "Sending FIN-ACK ..."
    send(t, verbose=False)

    
    print "Received Ack.\n"
    print "Done"


if __name__ == "__main__":
    main()