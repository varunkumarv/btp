import socket


TCP_IP = '10.10.10.4'
TCP_PORT = 55556
BUFFER_SIZE = 20  # Normally 1024, but we want fast response

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(5)

while True:
    conn, addr = s.accept()
    print 'Connection address:', addr
    conn.close()
