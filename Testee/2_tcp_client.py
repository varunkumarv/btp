#!/usr/bin/env python

import socket
import sys

def main():
    TCP_IP = '10.10.10.10'    
    if len(sys.argv) > 1:
        TCP_IP = sys.argv[1]
        
    TCP_PORT = 55556
    BUFFER_SIZE = 1024
    MESSAGE = "Hello, World!"

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TCP_IP, TCP_PORT))
    data = s.recv(BUFFER_SIZE)
    print "received data:", data
    s.close()


if __name__ == "__main__":
    main()