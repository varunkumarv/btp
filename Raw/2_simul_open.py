from scapy.all import IP, TCP, sr1, sniff, send
import sys
import socket
import fcntl, struct, os


sys.path.insert(0,"../Common-Functions")
from defns import *
from common_fns import *
import common_fns


def get_timestamp_val(options_list):
    for opt,val in options_list:
        if opt == 'Timestamp':
            return val[0]


def get_filter(iface):
    host_ip = get_host_ip(iface)
    filter_items = ["tcp and port", str(ports["srv_port"]), "and host", host_ip]
    return " ".join(filter_items)


def main():
    iface = 'wlan0'
    if len(sys.argv) == 2:
        iface = sys.argv[1]

    server_ip = get_host_ip(iface)
    filter = get_filter(iface)

    print "\nWaiting for SYN from client ..."
    syn = sniff(iface="wlan0", filter=get_filter(iface), count=1)

    ip_packet = IP()
    ip_packet.src = server_ip
    ip_packet.dst = syn[0][IP].src

    tcp_packet = TCP()
    tcp_packet.dport = syn[0][TCP].sport
    tcp_packet.sport = ports["srv_port"]
    tcp_packet.flags = "S"
    tcp_packet.seq = tcp_vals["ISN"]
    tcp_packet.options = [('Timestamp', (tcp_vals["TSval"], 0))]

    t = ip_packet/tcp_packet
    print"Received SYN, sending SYN ... "

    rep = sr1(t, verbose=False)
    if rep:
        tcp_packet.flags = "SA"
        tcp_packet.ack = rep[TCP].seq + 1
        tcp_packet.seq = tcp_vals["ISN"]
        tcp_packet.options = [ ('Timestamp', (tcp_vals["TSval"], get_timestamp_val(syn[0][TCP].options))) ]
        t = ip_packet/tcp_packet
        print "Received SYN+ACK, sending SYN+ACK ...\n"
        send(t, verbose=False)

    tcp_packet.flags = "A"
    tcp_packet.ack = rep[0][TCP].seq + 1
    tcp_packet.seq = tcp_vals["ISN"]+1
    tcp_packet.options = [ ('Timestamp', (tcp_vals["TSval"], get_timestamp_val(rep[0][TCP].options))) ]
    t = ip_packet/tcp_packet/"Varun"
    print "Sending Data ..."
    send(t, verbose=False)

    while rep[0][TCP].ack != tcp_vals["ISN"] + 6:
        rep = sniff(iface="wlan0", filter=get_filter(iface), count=1)
    print "Received Acknowledgement ...\n"

    while rep[0][TCP].flags & flags["FIN"] == False:
        rep = sniff(iface="wlan0", filter=get_filter(iface), count=1)
    print "Received FIN ..."

    tcp_packet.flags = "A"
    tcp_packet.ack = rep[0][TCP].seq + 1
    tcp_packet.seq = tcp_vals["ISN"] + 6
    tcp_packet.options = [ ('Timestamp', (tcp_vals["TSval"], get_timestamp_val(rep[0][TCP].options))) ]
    t = ip_packet/tcp_packet
    print "Sending Ack for FIN Received ...\n"
    send(t, verbose=False)

    tcp_packet.flags = "FA"
    tcp_packet.ack = rep[0][TCP].seq + 1
    tcp_packet.seq = tcp_vals["ISN"] + 6
    tcp_packet.options = [ ('Timestamp', (tcp_vals["TSval"], get_timestamp_val(rep[0][TCP].options))) ]
    t = ip_packet/tcp_packet
    print "Sending Fin-Ack ..."
    send(t, verbose=False)
    
    print "Received Ack.\n"
    print "Done"


if __name__ == "__main__":
    main()