import socket
import sys
import time

sys.path.insert(0,"../Common-Functions")
from common_fns import *
from defns import *

def main():
    target_ip = "10.10.10.10"
    if len(sys.argv) > 1:
        target_ip = sys.argv[1]
    target_port = ports["cli_port"]
    target = (target_ip, target_port)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(target)
    time.sleep(2)
    s.shutdown(socket.SHUT_WR)
    data = s.recv(10)
    print "Received data: Even after sending FIN from my side, success!!", data
    time.sleep(1)
    s.close()

if __name__ == "__main__":
    main()
