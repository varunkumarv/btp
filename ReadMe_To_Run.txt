Test-1
    Testing TCP Passive Open
        ping from Machine-1 to DUT
        DUT: python 3_server.py
        Machine-1: sudo python 3_norm_open.py interface_name server_ip_addr

Test-2
    Testing TCP Active Open
        Machine-1: sudo python 1_norm_passive.py interface_name server_ip_addr
        DUT: python 1_cli_active.py machine-1_addr

Test-3
    Testing TCP Simultaneous Open
        Machine-1: sudo python 2_simul_open.py [IFACE]
        DUT: 2_tcp_client.py machine-1_addr 
    
Test-4
    Testing Normal TCP Connection Breakdown
        ping from Machine-1 to DUT
        DUT: python 4_server.py
        Machine-1: sudo python 4_norm_close.py interface_name server_ip_addr

Test-5
    Testing Simultaneous TCP Connection Breakdown
        ping from Machine-1 to DUT
        DUT: python 5_server.py
        Machine-1: sudo python 5_simul_close.py interface_name server_ip_addr

Test-6
    Testing Half-Close Receive
        ping from Machine-1 to DUT
        Machine-1 with Optional Argument: sudo python 6_half_close_send.py interface_name
        DUT: python 6_half_close_recv.py ip_addr_machine-1

Test-7
    Testing Half-Close Send
        ping from Machine-1 to DUT
        Machine-1 with Optional Argument: sudo python 7_half_close_recv.py interface_name
        DUT: python 7_half_close_send.py ip_addr_machine-1

Test-8
    TCP TIME_WAIT
        ping from Machine-1 to DUT
        Machine-1 with Optional Argument: sudo python 8_time_wait.py interface_name
        DUT: python 8_client_tw.py ip_addr_machine-1

Test-9
    TCP Repacketisation
        ping from Machine-1 to DUT
        Machine-1 with optional Argument: sudo python 9_srv_repak.py interface_name
        DUT: python 9_cli_repak.py

Test-10
    TCP Reset Packets
        Case-I Non-Existent Port
            Machine-1: python 9_srv_repak.py DUT_IP_ADDR

        Case-II Aborting existing Connection
