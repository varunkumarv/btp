from scapy.all import IP, TCP, sr1, sniff, send
import sys
import socket
import fcntl, struct, os
import random
import time

sys.path.insert(0,"../Common-Functions")
from defns import *
from common_fns import *


def get_timestamp_val(options_list):
    for opt,val in options_list:
        if opt == 'Timestamp':
            return val[0]


def get_filter(iface):
    host_ip = get_host_ip(iface)
    filter_items = ["tcp and port", str(ports["srv_port"]), "and host", host_ip]
    return " ".join(filter_items)


def main():
    iface = "wlan0"
    if len(sys.argv) > 1:
        iface = sys.argv[1]

    self_ip = get_host_ip(iface)
    filter = get_filter(iface)

    print "Waiting for SYN ..."
    while True:
        syn = sniff(iface="wlan0", filter=get_filter(iface), count=1)
        if syn[0][TCP].flags & flags["SYN"] == flags["SYN"]:
            break

    ip_packet = IP()
    ip_packet.src = self_ip
    ip_packet.dst = syn[0][IP].src

    tcp_packet = TCP()
    tcp_packet.dport = syn[0][TCP].sport
    tcp_packet.sport = ports["srv_port"]
    tcp_packet.flags = "SA"
    tcp_packet.seq = tcp_vals["ISN"]
    tcp_packet.ack = syn[0][TCP].seq + 1
    tcp_packet.options = [('Timestamp', (tcp_vals["TSval"], get_timestamp_val(syn[0][TCP].options) ))]

    t = ip_packet/tcp_packet
    print"Received SYN, sending SYN-ACK ... "
    send(t, verbose=False)

    ack = sniff(iface="wlan0", filter=get_filter(iface), count=1)
    print len(ack[0][TCP].payload)
    print "Received ACK for SYN-ACK"

    tcp_packet.flags = "FA"
    tcp_packet.ack = syn[0][TCP].seq
    tcp_packet.seq = tcp_vals["ISN"] + 1
    tcp_packet.options = [ ('Timestamp', (tcp_vals["TSval"], get_timestamp_val(syn[0][TCP].options))) ]
    t = ip_packet/tcp_packet
    print "Sending FIN for partial shutdown ..."
    send(t, verbose=False)

#    ack = sniff(iface="wlan0", filter=get_filter(iface), count=1)
    print 'Received ACK for our FIN'

    pkt = ""
    while True:
        pkt = sniff(iface="wlan0", filter=get_filter(iface), count=1)
        if len(pkt[0][TCP].payload) > 0:        
            print "Received Data, experiment successful: ", pkt[0][TCP].payload
            break

    tcp_packet.flags = "A"
    tcp_packet.ack = pkt[0][TCP].seq + len(pkt[0][TCP].payload)
    tcp_packet.seq = tcp_vals["ISN"] + 2
    tcp_packet.options = [ ('Timestamp', (tcp_vals["TSval"], get_timestamp_val(pkt[0][TCP].options))) ]
    t = ip_packet/tcp_packet
    print "Sending ACK for Data received ..."
    send(t, verbose=False)

    fin = sniff(iface="wlan0", filter=get_filter(iface), count=1)
    print 'Received FIN, sending ACK'

    tcp_packet.flags = "A"
    tcp_packet.ack = fin[0][TCP].seq + 1
    tcp_packet.seq = tcp_vals["ISN"] + 2
    tcp_packet.options = [ ('Timestamp', (tcp_vals["TSval"], get_timestamp_val(fin[0][TCP].options))) ]
    t = ip_packet/tcp_packet
    print "Sending final ACK ..."
    send(t, verbose=False)

    print "\nDone."


if __name__ == "__main__":
    main()
