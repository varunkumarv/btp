from scapy.all import IP, TCP, sr1, sniff, send
import sys
import socket
import fcntl, struct, os

iface = 'wlan0'
if len(sys.argv) == 2:
    iface = sys.argv[1]

server_port = 55556

initial_sequence = 1000
TSval = 12345


def get_timestamp_val(options_list):
    for opt,val in options_list:
        if opt == 'Timestamp':
            return val[0]


def get_host_ip(iface):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', "wlan0")
    )[20:24])


def get_filter():
    #tcp and port 55556 and host 10.10.10.10
    host_ip = get_host_ip(iface)
    filter_items = ["tcp and port", str(server_port), "and host", host_ip]
    return " ".join(filter_items)


if len(sys.argv) > 1 and sys.argv[1] == "server":

    server_ip = get_host_ip(iface)
    filter = get_filter()
    print server_ip, filter

    print "Waiting for SYN from client ..."
    syn = sniff(iface="wlan0", filter=get_filter(), count=1)

    ip_packet = IP()
    ip_packet.src = server_ip
    ip_packet.dst = syn[0][IP].src

    tcp_packet = TCP()
    tcp_packet.dport = syn[0][TCP].sport
    tcp_packet.sport = server_port
    tcp_packet.flags = "S"
    tcp_packet.seq = initial_sequence
    tcp_packet.options = [('Timestamp', (TSval, 0))]

    t = ip_packet/tcp_packet
    print"Received client SYN, sending server SYN ... "

    rep = sr1(t, verbose=False)
    if rep:
        tcp_packet.flags = "SA"
        tcp_packet.ack = rep[TCP].seq + 1
        tcp_packet.seq = initial_sequence
        tcp_packet.options = [ ('Timestamp', (TSval, get_timestamp_val(rep[TCP].options))) ]
        t = ip_packet/tcp_packet
        print "Received client SYN+ACK, sending SYN+ACK ..."
        send(t, verbose=False)
        print "Done."

    while True:
        rep = sniff(iface="wlan0", filter=get_filter(), count=1)
        if rep[0][TCP].flags == "FA":
            break

    tcp_packet.flags = "A"
    tcp_packet.ack = rep[0][TCP].seq + 1
    tcp_packet.seq = initial_sequence+1
    tcp_packet.options = [ ('Timestamp', (TSval, get_timestamp_val(rep[0][TCP].options))) ]
    t = ip_packet/tcp_packet/"Varun"
    print "Sending Data ..."
    send(t, verbose=False)

    rep = sniff(iface="wlan0", filter=get_filter(), count=1)

    # tcp_packet.flags = "A"
    # tcp_packet.ack = rep[0][TCP].seq
    # tcp_packet.seq = initial_sequence + 6
    # tcp_packet.options = [ ('Timestamp', (TSval, get_timestamp_val(rep[0][TCP].options))) ]
    # t = ip_packet/tcp_packet
    # print "Sending Ack for Data ..."
    # send(t, verbose=False)

    # rep = sniff(iface="wlan0", filter=get_filter(), count=1)

    tcp_packet.flags = "FA"
    tcp_packet.ack = rep[0][TCP].seq
    tcp_packet.seq = initial_sequence + 6
    tcp_packet.options = [ ('Timestamp', (TSval, get_timestamp_val(rep[0][TCP].options))) ]
    t = ip_packet/tcp_packet
    print "Sending Fin-Ack ..."
    send(t, verbose=False)
    print "Done."
    
#    print data[0][TCP].show()
#    print data[0][TCP].payload
    exit(0)
