import socket
import sys
import time

sys.path.insert(0,"../Common-Functions")
from common_fns import *
from defns import *

def main():
    target_ip = "10.10.10.10"
    if len(sys.argv) > 1:
        target_ip = sys.argv[1]
    target_port = ports["srv_port"]
    target = (target_ip, target_port)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(target)
    time.sleep(3)
    data = s.send("Varun")
    time.sleep(3)
    s.close()

if __name__ == "__main__":
    main()
