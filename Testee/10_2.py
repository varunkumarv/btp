import socket
import sys
import time

sys.path.insert(0,"../Common-Functions")
from common_fns import *
from defns import *

def main():
    iface = "wlan0"
    if len(sys.argv) > 1:
        iface = sys.argv[1]

    TCP_IP = get_host_ip(iface)
    TCP_PORT = ports["srv_port"]

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((TCP_IP, TCP_PORT))
    s.listen(1)

    conn, addr = s.accept()
    time.sleep(10)    

if __name__ == "__main__":
    main()